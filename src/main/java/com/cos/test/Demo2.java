package com.cos.test;

import org.activiti.engine.*;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.repository.ProcessDefinitionQuery;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;

import java.util.List;

/**
 * @author: COS
 * @time: 2023/1/10 11:14
 * @description:
 */
public class Demo2 {

    public static void main(String[] args) {

//        actTask();
//        queryTaskList();
//        for (int i = 0; i < 5; i++) {
//            createProcessInstance();
//
//        }
//        storeBpmn();
        actTask();
    }

    /**
     *
     * @author: COS
     * @return:
     * @time: 2023/1/10 15:15
     * @description: 执行任务
     */
    public static void actTask(){
        //1 获取activiti引擎
        ProcessEngine engine = ProcessEngines.getDefaultProcessEngine();
        //2 获取任务服务
        TaskService taskService = engine.getTaskService();
        //3 获取任务信息
        Task singleResult = taskService.createTaskQuery()
                .processDefinitionId("myProcess_1:1:7504")
                .taskAssignee("林志清")
                .singleResult();
        //4 通过任务服务来执行任务
        // 业务逻辑代码
        // 在操作完业务代码后,使用任务id来提交完成任务,完成任务
        taskService.complete(singleResult.getId());
        System.out.println(singleResult.getAssignee()+"执行任务"+singleResult.getName()+"完成");
    }

    /**
     *
     * @author: COS
     * @return:
     * @time: 2023/1/10 15:07
     * @description:
     */
    public static void queryTaskList(){
        //1 获取activiti引擎
        ProcessEngine engine = ProcessEngines.getDefaultProcessEngine();
        //2 获取任务服务
        TaskService taskService = engine.getTaskService();
        //3 使用任务服务,通过 流程定义id 和 任务执行人 查询 执行人有什么任务需要执行的
        List<Task> taskList = taskService.createTaskQuery()
                // 设置任务定义 id
                .processDefinitionId("myProcess_1:1:5004")
                // 设置任务执行人/指派人
                .taskAssignee("林志清")
                .list();
        //4 遍历任务列表
        for (Task task : taskList) {
            // 输出任务 信息
            //输出任务id
            System.out.println(task.getId());
            //输出任务 名称
            System.out.println(task.getName());
            //输出任务执行人/指派人
            System.out.println(task.getAssignee());
        }
    }



    /**
     *
     * @author: COS
     * @return:
     * @time: 2023/1/10 11:37
     * @description: 创建流程实例 ,流程实例被创建 就代表 开始了一个请假的申请流程了
     * 流程实例创建必须要有对应流程定义存在才可以,并且需要使用运行时服务来创建实例
     * 流程定义时保存在activiti数据库中,所以需要使用仓储服务来获取对应的流程实例
     */
    public static void createProcessInstance(){
        //1 获取activiti引擎
        ProcessEngine engine = ProcessEngines.getDefaultProcessEngine();
        //2 获取仓储服务, 用于获取流程定义备用
        RepositoryService repositoryService = engine.getRepositoryService();
        //3 获取流程定义对象
        ProcessDefinition processDefinition = repositoryService.createProcessDefinitionQuery()
                // 使用部署在activiti数据库中的bpmn来查询创建流程定义的对象
                .deploymentId("5001")
                // 单个结果,获取单个流程定义对象
                .singleResult();
        //4 通过engine 获取运行时服务
        RuntimeService runtimeService = engine.getRuntimeService();
        //5 通过运行时服务 创建一个流程实例,需要使用到流程定义的id
        ProcessInstance processInstance = runtimeService.createProcessInstanceBuilder()
                // 使用流程定义id 创建一个流程实例
                // 这里设置业务id
                .processDefinitionId(processDefinition.getId())
                .name("陆玲请假")
                .start();
        System.out.println(processInstance.getId());
        System.out.println(processInstance.getName());
        System.out.println(processInstance.getProcessDefinitionId());
    }

    /**
     *
     * @author: COS
     * @return:
     * @time: 2023/1/10 11:28
     * @description:
     * 部署bpmn 就是告诉activiti 我这里有这个bpmn的业务流程存在,后面activiti就可以根据这个部署下来的bpmn来
     * 进行创建流程定义->流程实例->一个个任务操作
     */
    public static void storeBpmn(){
        //1 获取activiti的引擎
        ProcessEngine engine = ProcessEngines.getDefaultProcessEngine();
        //2 通过activiti引擎获取 仓储服务
        RepositoryService repositoryService = engine.getRepositoryService();
        //3 创建不是
        Deployment deployment = repositoryService.createDeployment()
                // 使用文件地址名称 添加对应的资源进去
                .addClasspathResource("bpmn/first.bpmn")
                .addClasspathResource("bpmn/first.png")
                // 设置流程名称
                .name("qf2206请假流程")
                // 设置不检查xml
                .disableSchemaValidation()
                .deploy();
        System.out.println(deployment.getId());
        System.out.println(deployment.getKey());
        System.out.println(deployment.getName());
    }

}
