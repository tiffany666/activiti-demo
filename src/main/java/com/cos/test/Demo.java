package com.cos.test;

import org.activiti.engine.ProcessEngine;
import org.activiti.engine.ProcessEngineConfiguration;
import org.activiti.engine.ProcessEngines;

/**
 * @author: COS
 * @time: 2023/1/10 9:39
 * @description:
 */
public class Demo {
    public static void main(String[] args) {
        //读取配置文件
        ProcessEngineConfiguration configuration =
                ProcessEngineConfiguration.createProcessEngineConfigurationFromResource("activiti.cfg.xml");
        //获取引擎,根据配置文件会自动创建表
        ProcessEngine processEngine = configuration.buildProcessEngine();
    }
}
