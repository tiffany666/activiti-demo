package com.cos.test.entity;

import java.io.Serializable;

/**
 * @author: COS
 * @time: 2023/1/11 9:53
 * @description:
 */
public class Dept implements Serializable {
    private Integer dno;
    private String name;

    @Override
    public String toString() {
        return "Dept{" +
                "dno=" + dno +
                ", name='" + name + '\'' +
                '}';
    }

    public Integer getDno() {
        return dno;
    }

    public void setDno(Integer dno) {
        this.dno = dno;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Dept() {
    }

    public Dept(Integer dno, String name) {
        this.dno = dno;
        this.name = name;
    }
}
