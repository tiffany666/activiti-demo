import com.cos.test.entity.Dept;
import org.activiti.engine.ProcessEngine;
import org.activiti.engine.ProcessEngines;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.TaskService;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.junit.Test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author: COS
 * @time: 2023/1/11 9:00
 * @description:
 */
public class Demo4 {
    final String KEY= "myProcess_1";
    @Test
    public void d4() {
        ProcessEngines.getDefaultProcessEngine().
                getRepositoryService().
                deleteDeployment("80001", true);
    }


    @Test
    public void d5() {
        //1 获取activiti引擎
        ProcessEngine engine = ProcessEngines.getDefaultProcessEngine();
        //2 获取任务服务
        TaskService taskService = engine.getTaskService();
        //3 查询执行人的任务
        List<Task> list = taskService.createTaskQuery()
                .processDefinitionKey(KEY)
//                .taskAssignee("小王")
                .taskCandidateUser("小王")
                .list();
        for (Task task : list) {
            System.out.println(task.getName());
            System.out.println(task.getId());
            // 直接执行任务是可以的,但是这里做记录里面,就看不到是谁执行了当前的任务
            // 拾取任务-->简单的理解,就是进行绑定,绑定当前任务的执行人
//            taskService.claim(task.getId(),"小王");
            // 归回任务 ,把绑定的执行人重新设置为null,就可以让其他的候选人去重新选择这个任务执行
//            taskService.claim(task.getId(),null);
            // 指派执行人 来执行这个任务
            taskService.setAssignee(task.getId(),"小李123");
//            taskService.complete(task.getId());
        }
        //4 输出任务信息
//        System.out.println(task.getName());
//        System.out.println(task.getId());
//        if (task != null) {
//            //这个执行人有任务可以执行
//            taskService.complete(task.getId());
//        }
    }

    @Test
    public void d2() {
        // 1 获取activiti引擎
        ProcessEngine engine = ProcessEngines.getDefaultProcessEngine();
        // 2 获取运行时服务
        Map<String, Object> map = new HashMap<>();
        // ${user1}
        map.put("user1", "迪丽热巴");
        map.put("user2", "张国荣");
        ProcessInstance my_leave = engine.getRuntimeService()
                .startProcessInstanceByKey(KEY, map);
        System.out.println(my_leave.getProcessDefinitionKey());
        System.out.println(my_leave.getProcessDefinitionId());
        System.out.println(my_leave.getProcessDefinitionName());
        System.out.println(my_leave.getProcessVariables());

    }

    @Test
    public void d1() {
        //1 获取activit引擎
        ProcessEngine engine = ProcessEngines.getDefaultProcessEngine();
        //2 获取仓储服务
        RepositoryService repositoryService = engine.getRepositoryService();
        //3 加入资源
        Deployment deploy = repositoryService.createDeployment()
                .addClasspathResource("bpmn2/Leave3.bpmn")
                .addClasspathResource("bpmn2/Leave3.png")
                .name("请假流程-组任务")
                .disableSchemaValidation()
                .deploy();
        System.out.println(deploy.getName());
        System.out.println(deploy.getId());
    }
}
