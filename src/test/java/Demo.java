import org.activiti.engine.ProcessEngine;
import org.activiti.engine.ProcessEngines;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.TaskService;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

/**
 * @author: COS
 * @time: 2023/1/11 9:00
 * @description:
 */
public class Demo {

    @Test
    public void d3(){
        //1 获取activiti引擎
        ProcessEngine engine = ProcessEngines.getDefaultProcessEngine();
        //2 获取任务服务
        TaskService taskService = engine.getTaskService();
        //3 查询执行人的任务
        Task task = taskService.createTaskQuery()
                .processDefinitionKey("my_leave")
                .taskAssignee("小李")
                .singleResult();
        //4 输出任务信息
        System.out.println(task.getName());
        System.out.println(task.getId());
    }
    @Test
    public void d2(){
        // 1 获取activiti引擎
        ProcessEngine engine = ProcessEngines.getDefaultProcessEngine();
        // 2 获取运行时服务
        Map<String,Object> map = new HashMap<>();
        // ${user1}
        map.put("user1","小李");
        map.put("user2","小红");
        map.put("user3","小陈");
        ProcessInstance my_leave = engine.getRuntimeService()
                .startProcessInstanceByKey("my_leave", map);
        System.out.println(my_leave.getProcessDefinitionKey());
        System.out.println(my_leave.getProcessDefinitionId());
        System.out.println(my_leave.getProcessDefinitionName());
        System.out.println(my_leave.getProcessVariables());

    }
    @Test
    public void d1(){
        //1 获取activit引擎
        ProcessEngine engine = ProcessEngines.getDefaultProcessEngine();
        //2 获取仓储服务
        RepositoryService repositoryService = engine.getRepositoryService();
        //3 加入资源
        Deployment deploy = repositoryService.createDeployment()
                .addClasspathResource("bpmn2/Leave.bpmn")
                .addClasspathResource("bpmn2/Leave.png")
                .name("请假流程-动态执行人")
                .disableSchemaValidation()
                .deploy();
        System.out.println(deploy.getName());
        System.out.println(deploy.getId());
    }
}
