package gateway;

import org.activiti.engine.*;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

/**
 * @author: COS
 * @time: 2023/1/11 16:47
 * @description:
 */
public class TestInclu {
    @Test
    public void tt(){
//        t1();
//        t2();
        t3();
    }

    public static void t1() {
        ProcessEngine engine = ProcessEngines.getDefaultProcessEngine();
        RepositoryService repositoryService = engine.getRepositoryService();
        Deployment deploy = repositoryService.createDeployment()
                .addClasspathResource("bpmn3/GateWayLeave3.bpmn")
                .addClasspathResource("bpmn3/GateWayLeave3.png")
                .disableSchemaValidation()
                .name("包含网关")
                .deploy();
    }

    public static void t2() {
        ProcessEngine defaultProcessEngine = ProcessEngines.getDefaultProcessEngine();
        RuntimeService runtimeService = defaultProcessEngine.getRuntimeService();
        Map<String, Object> map = new HashMap<>();
        // 设置一个2个条件都成立的值
        map.put("day", 100);
        ProcessInstance processInstance = runtimeService.
                startProcessInstanceByKey("myProcess_key3", map);
    }

    public static void t3() {
        ProcessEngine defaultProcessEngine = ProcessEngines.getDefaultProcessEngine();
        TaskService taskService = defaultProcessEngine.getTaskService();
        Task t = taskService.createTaskQuery()
                .processDefinitionKey("myProcess_key3")
                .taskAssignee("总经理")
                .singleResult();
        System.out.println(t);
        if (t != null) {
//            taskService.setVariable(t.getId(),"day", 3);
            taskService.complete(t.getId());
        }
    }
}
