import org.activiti.bpmn.model.BpmnModel;
import org.activiti.bpmn.model.FlowElement;
import org.activiti.bpmn.model.FlowNode;
import org.activiti.bpmn.model.SequenceFlow;
import org.activiti.engine.*;
import org.activiti.engine.history.HistoricActivityInstance;
import org.activiti.engine.history.HistoricProcessInstance;
import org.activiti.engine.history.HistoricTaskInstance;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.junit.Test;

import java.util.*;

/**
 * @author: COS
 * @time: 2023/1/11 9:00
 * @description:
 */
public class Demo5 {
    final String KEY = "my_leave_key";

    @Test
    public void d4() {
        ProcessEngines.getDefaultProcessEngine().
                getRepositoryService().
                deleteDeployment("127501", true);
    }


    @Test
    public void d7() {
        //1 获取activiti引擎
        ProcessEngine engine = ProcessEngines.getDefaultProcessEngine();
        ProcessInstance processInstance = engine.getRuntimeService().createProcessInstanceQuery()
                .processDefinitionKey(KEY).singleResult();
        //2 获取所有历史节点数据
        HistoryService historyService = engine.getHistoryService();
        TaskService taskService = engine.getTaskService();
        //3 获取任务节点的历史信息,通过实例id获取
        List<HistoricActivityInstance> list = historyService.createHistoricActivityInstanceQuery()
                .processInstanceId(processInstance.getId())
                .orderByHistoricActivityInstanceStartTime()
                .asc()
                .list();
        for (HistoricActivityInstance historicTaskInstance : list) {
            System.out.println(historicTaskInstance.getId());
        }
        // 记录第一个任务节点
        HistoricActivityInstance historicActivityInstance = list.get(0);
        // 记录当前的任务节点
        HistoricActivityInstance historicActivityInstance1 = list.get(list.size() - 1);
        // 获取bpmn的模型对象
        BpmnModel bpmnModel = engine.getRepositoryService().getBpmnModel(historicActivityInstance.getProcessDefinitionId());
        // 获取第一个模型节点
        FlowNode firstElement = (FlowNode) bpmnModel.getMainProcess().getFlowElement(historicActivityInstance.getActivityId());
        System.out.println(firstElement.getName());
        // 获取当前模型节点
        FlowNode currentElement = (FlowNode) bpmnModel.getMainProcess().getFlowElement(historicActivityInstance1.getActivityId());
        //记录原活动方向
        List<SequenceFlow> oriSequenceFlows = new ArrayList<>(currentElement.getOutgoingFlows());
        //清理活动方向
        currentElement.getOutgoingFlows().clear();
        //建立新的方向
        List<SequenceFlow> newSequenceFlows = new ArrayList<>();
        SequenceFlow newSequenceFlow = new SequenceFlow();
        String uuid = UUID.randomUUID().toString().replace("-", "");
        newSequenceFlow.setId(uuid);
        newSequenceFlow.setSourceFlowElement(currentElement);  //原节点
        newSequenceFlow.setTargetFlowElement(firstElement);  //目标节点
        newSequenceFlows.add(newSequenceFlow);
        currentElement.setOutgoingFlows(newSequenceFlows);
        Task task = taskService.createTaskQuery()
                .processDefinitionKey(KEY)
                .taskAssignee("张国荣")
                .singleResult();
        //完成节点任务
        //taskService.complete(task.getId(), map条件);动态每次完成的时候设置进去条件，不用一次性开始就设置
        taskService.complete(task.getId());//驳回不需要条件
        //恢复原方向
        currentElement.setOutgoingFlows(oriSequenceFlows);
    }

    @Test
    public void d6() {
        //1 获取activiti引擎
        ProcessEngine engine = ProcessEngines.getDefaultProcessEngine();
        //2 获取任务服务
        TaskService taskService = engine.getTaskService();
        //3 查询执行人的任务
        List<Task> list = taskService.createTaskQuery()
                .processDefinitionKey(KEY)
                .taskAssignee("迪丽热巴")
                .list();
        for (Task task : list) {
            System.out.println(task.getName());
            System.out.println(task.getId());
            taskService.complete(task.getId());
        }
    }

    @Test
    public void d5() {
        //1 获取activiti引擎
        ProcessEngine engine = ProcessEngines.getDefaultProcessEngine();
        //2 获取任务服务
        TaskService taskService = engine.getTaskService();
        //3 查询执行人的任务
        List<Task> list = taskService.createTaskQuery()
                .processDefinitionKey(KEY)
                .taskAssignee("小李")
//                .taskCandidateUser("迪丽热巴")
                .list();
        for (Task task : list) {
            System.out.println(task.getName());
            System.out.println(task.getId());
            // 设置驳回,在回退节点重新设置回默认的0
            taskService.setVariable(task.getId(), "flag", 0);
            taskService.complete(task.getId());
        }
        //4 输出任务信息
//        System.out.println(task.getName());
//        System.out.println(task.getId());
//        if (task != null) {
//            //这个执行人有任务可以执行
//            taskService.complete(task.getId());
//        }
    }

    @Test
    public void d2() {
        // 1 获取activiti引擎
        ProcessEngine engine = ProcessEngines.getDefaultProcessEngine();
        // 2 获取运行时服务
        Map<String, Object> map = new HashMap<>();
        // ${user1}
        map.put("user1", "迪丽热巴");
        map.put("user2", "张国荣");
        map.put("user3", "孙依云");
        ProcessInstance my_leave = engine.getRuntimeService()
                .startProcessInstanceByKey(KEY, map);
        System.out.println(my_leave.getProcessDefinitionKey());
        System.out.println(my_leave.getProcessDefinitionId());
        System.out.println(my_leave.getProcessDefinitionName());
        System.out.println(my_leave.getProcessVariables());

    }

    @Test
    public void d1() {
        //1 获取activit引擎
        ProcessEngine engine = ProcessEngines.getDefaultProcessEngine();
        //2 获取仓储服务
        RepositoryService repositoryService = engine.getRepositoryService();
        //3 加入资源
        Deployment deploy = repositoryService.createDeployment()
                .addClasspathResource("bpmn2/Leave5.bpmn")
                .addClasspathResource("bpmn2/Leave5.png")
                .name("请假流程-回退测试2")
                .disableSchemaValidation()
                .deploy();
        System.out.println(deploy.getName());
        System.out.println(deploy.getId());
    }
}
