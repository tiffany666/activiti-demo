import com.cos.test.entity.Dept;
import org.activiti.engine.ProcessEngine;
import org.activiti.engine.ProcessEngines;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.TaskService;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.junit.Test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author: COS
 * @time: 2023/1/11 9:00
 * @description:
 */
public class Demo3 {

    @Test
    public void d4() {
        ProcessEngines.getDefaultProcessEngine().getRepositoryService().deleteDeployment("50001", true);
    }

    @Test
    public void d3() {
        //1 获取activiti引擎
        ProcessEngine engine = ProcessEngines.getDefaultProcessEngine();
        //2 获取任务服务
        TaskService taskService = engine.getTaskService();
        //3 查询执行人的任务
        List<Task> list = taskService.createTaskQuery()
                .processDefinitionKey("my_leave2")
                .taskAssignee("小红")
                .list();
        //4 输出任务信息
//        System.out.println(task.getName());
//        System.out.println(task.getId());
        for (Task task : list) {
            if (task != null) {
                if (task.getName().equals("创建请假单")) {
                    taskService.setVariableLocal(task.getId(), "wahaha", 20);
                } else {
                    System.out.println("============================");
                    Integer wahaha = taskService.getVariableLocal(task.getId(), "wahaha", int.class);
                    System.out.println(wahaha);
                }
                //这个执行人有任务可以执行
                taskService.complete(task.getId());
            }
        }
    }

    @Test
    public void d5() {
        //1 获取activiti引擎
        ProcessEngine engine = ProcessEngines.getDefaultProcessEngine();
        //2 获取任务服务
        TaskService taskService = engine.getTaskService();
        //3 查询执行人的任务
        Task task = taskService.createTaskQuery()
                .processDefinitionKey("my_leave2")
                .taskAssignee("小陈")
                .singleResult();
        //4 输出任务信息
//        System.out.println(task.getName());
//        System.out.println(task.getId());
        if (task != null) {
            System.out.println("============================");
            System.out.println("============================");
            System.out.println("============================");
            System.out.println("============================");
            // 这个局部变量只能在当前任务内有效
//            taskService.setVariable(task.getId(), "wahaha", "20");
            String wahaha = taskService.getVariable(task.getId(), "wahaha", String.class);
            System.out.println(wahaha);
            System.out.println("============================");
            System.out.println("============================");
            System.out.println("============================");
            System.out.println("============================");
            //这个执行人有任务可以执行
            taskService.complete(task.getId());
        }
    }

    @Test
    public void d2() {
        // 1 获取activiti引擎
        ProcessEngine engine = ProcessEngines.getDefaultProcessEngine();
        // 2 获取运行时服务
        Map<String, Object> map = new HashMap<>();
        // ${user1}
        map.put("user1", "小李");
        map.put("user2", "小红");
        map.put("user3", "小陈");
        map.put("user4", "人事");
        map.put("user5", "java经理");
        map.put("user6", "python经理");
        // 设置分支判断使用的变量
        map.put("day", 5);
        // ${dept.name=='java'}
        Dept d = new Dept(1, "php");
        // key 是dept
        map.put("dept", d);
        ProcessInstance my_leave = engine.getRuntimeService()
                .startProcessInstanceByKey("my_leave2", map);
        System.out.println(my_leave.getProcessDefinitionKey());
        System.out.println(my_leave.getProcessDefinitionId());
        System.out.println(my_leave.getProcessDefinitionName());
        System.out.println(my_leave.getProcessVariables());

    }

    @Test
    public void d1() {
        //1 获取activit引擎
        ProcessEngine engine = ProcessEngines.getDefaultProcessEngine();
        //2 获取仓储服务
        RepositoryService repositoryService = engine.getRepositoryService();
        //3 加入资源
        Deployment deploy = repositoryService.createDeployment()
                .addClasspathResource("bpmn2/Leave2.bpmn")
                .addClasspathResource("bpmn2/Leave2.png")
                .name("请假流程-任务分支")
                .disableSchemaValidation()
                .deploy();
        System.out.println(deploy.getName());
        System.out.println(deploy.getId());
    }
}
